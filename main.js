document.querySelector('.menu-btn').addEventListener('click', () =>{
    document.querySelector('.nav-menu').classList.toggle('show');
});
ScrollReveal().reveal('.showcase');
ScrollReveal().reveal('.cards', {delay: 200});
ScrollReveal().reveal('.cards-banner-one', {delay: 200});
ScrollReveal().reveal('.social', {delay: 200});
ScrollReveal().reveal('.footer-links', {delay: 200});